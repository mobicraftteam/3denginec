#include "stdafx.h"
#include "Primitives.h"

Mesh  Primitives::CreateBox(float size)
{
	Mesh cubeMesh;

	cubeMesh.AddVertex(Vector3(-size, size, -size));
	cubeMesh.AddVertex(Vector3(-size, -size, -size));
	cubeMesh.AddVertex(Vector3(size, -size, -size));
	cubeMesh.AddTriangle(0);

	cubeMesh.AddVertex(Vector3(size, -size, -size));
	cubeMesh.AddVertex(Vector3(size, size, -size));
	cubeMesh.AddVertex(Vector3(-size, size, -size));
	cubeMesh.AddTriangle(1);

	cubeMesh.AddVertex(Vector3(-size, size, -size));
	cubeMesh.AddVertex(Vector3(-size, size, size));
	cubeMesh.AddVertex(Vector3(-size, -size, size));
	cubeMesh.AddTriangle(1);

	cubeMesh.AddVertex(Vector3(-size, -size, size));
	cubeMesh.AddVertex(Vector3(-size, -size, -size));
	cubeMesh.AddVertex(Vector3(-size, size, size));
	cubeMesh.AddTriangle(1);

	cubeMesh.AddVertex(Vector3(size, size, -size));
	cubeMesh.AddVertex(Vector3(size, size, size));
	cubeMesh.AddVertex(Vector3(size, -size, size));
	cubeMesh.AddTriangle(1);

	cubeMesh.AddVertex(Vector3(size, -size, size));
	cubeMesh.AddVertex(Vector3(size, -size, -size));
	cubeMesh.AddVertex(Vector3(size, size, size));
	cubeMesh.AddTriangle(1);

	cubeMesh.AddVertex(Vector3(-size, size, size));
	cubeMesh.AddVertex(Vector3(-size, -size, size));
	cubeMesh.AddVertex(Vector3(size, -size, size));
	cubeMesh.AddTriangle(0);

	cubeMesh.AddVertex(Vector3(size, -size, size));
	cubeMesh.AddVertex(Vector3(size, size, size));
	cubeMesh.AddVertex(Vector3(-size, size, size));
	cubeMesh.AddTriangle(1);

	return cubeMesh;
}