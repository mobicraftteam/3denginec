#include "stdafx.h"
#include "RenderEngine.h"
#include "Renderer.h"
#include <string>
#include "Camera.h"
#include <iostream>
#include <conio.h>
#include "WolfEngine.h"

string intToStr(int n)
{
	string tmp;
	if (n < 0) {
		tmp = "-";
		n = -n;
	}
	if (n > 9)
		tmp += intToStr(n / 10);
	tmp += n % 10 + 48;
	return tmp;
}

RenderEngine::RenderEngine(int w, int h, bool thR)
{
	//multithreadRendering = thR;

	height = h;
	width = w;
	
	hasCamera = false;

	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);

	wHnd = GetStdHandle(STD_OUTPUT_HANDLE);
	rHnd = GetStdHandle(STD_INPUT_HANDLE);

	//KURSOR
	CONSOLE_CURSOR_INFO info;
	info.dwSize = 100;
	info.bVisible = false;
	SetConsoleCursorInfo(wHnd, &info);
	//

	CONSOLE_FONT_INFOEX font{ sizeof(CONSOLE_FONT_INFOEX) };;
	GetCurrentConsoleFontEx(wHnd, false, &font);

	resolution_x = w / font.dwFontSize.X;
	resolution_y = h / font.dwFontSize.Y;

	screenSize = Vector2(resolution_x, resolution_y);

	bufferSize = { resolution_x,resolution_y};
	SetConsoleScreenBufferSize(wHnd, bufferSize);

	SMALL_RECT windowSize = { 0, 0, resolution_x-1, resolution_y-1 };
	SetConsoleWindowInfo(wHnd, TRUE, &windowSize);

	charBufSize = { resolution_x, resolution_y };
	characterPos = { 0,0 };
	writeArea = { 0,0,resolution_x - 1,resolution_y - 1};

	for (int y = 0; y < resolution_y; ++y) {
		for (int x = 0; x < resolution_x; ++x) {
			clearBuffer[x + (resolution_x * y)].Char.AsciiChar = 00;
			clearBuffer[x + (resolution_x * y)].Attributes = 0;
		}
	}
}

void RenderEngine::InitEngine()
{
	if (!hasCamera)
	{
		system("cls");
		std::cout << "NO CAMERA ON SCENE!" << std::endl;
		std::cout << "ADD CAMERA AND REBUILD APPLICATION!" << std::endl;
		_getch();
	}
	else
	{
		system("cls");
	}
}


void RenderEngine::ClearBuffer()
{
	std::copy(clearBuffer, clearBuffer + (resolution_x*resolution_y), screenBuffer);
}

void RenderEngine::Draw()
{
	WriteConsoleOutputA(wHnd, screenBuffer, charBufSize, characterPos, &writeArea);
}

void RenderEngine::AddRenderer(Renderer *r)
{
	renderers.push_back(r);
}

void RenderEngine::DrawPixel(Vector2 pos, CHAR_INFO *pixel)
{
	if (pos.x < resolution_x && pos.y < resolution_y && pos.x>=0 && pos.y>=0)
	{
		pixelDrawPos = pos.x + (resolution_x * pos.y);
		screenBuffer[pixelDrawPos] = *pixel;
	}
}

void RenderEngine::Render()
{
	for (size_t i = 0; i < renderers.size(); i++)
	{
		renderers[i]->RenderMesh();
	}

}

void RenderEngine::MainRenderEngineUpdate()
{
	if (hasCamera)
	{
		ClearBuffer();
		Render();
		//WolfEngine::mainEngine->DrawFps(); // BROKEN PERFORMANCE
		Draw();
		
	}
}

void RenderEngine::GoFullScreen()
{
	system("cls");
	SetConsoleDisplayMode(wHnd, CONSOLE_FULLSCREEN_MODE, 0);
}


