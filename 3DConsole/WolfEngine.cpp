#include "stdafx.h"
#include "WolfEngine.h"
#include <string>
#include "RenderEngine.h"
#include <iostream>
#include <conio.h>
#include "Object.h"
#include "Renderer.h"
#include <conio.h>
#include "Camera.h"
#include <chrono>

WolfEngine::WolfEngine()
{
	renderEngine = new RenderEngine(1920,1080,false);
	exitEngine = false;
	timePerFrame = 1 /320.0;
	FPSPixel = new CHAR_INFO();
	pos = new Vector2(0, 0);
}

WolfEngine::WolfEngine(int fTimeStep)
{
	fixedTimeStep = fTimeStep;
	renderEngine = new RenderEngine(1920, 1080, false);
	exitEngine = false;
}


void WolfEngine::Init()
{
	int choice = 0;
	while (choice!=53)
	{
		std::cout << "WolfEngine v0.0.01a" << std::endl;
		std::cout << "Created by Artur 'Wolf' Czapiewski" << std::endl;
		std::cout << "www.wolfstudio.xyz | artur.czapiewski@yahoo.com" << std::endl;
		std::cout << "MENU" << std::endl;
		std::cout << "1.Cube Benchmark" << std::endl;
		std::cout << "2.Double Cubes" << std::endl;
		std::cout << "3.Load Model" << endl;
		std::cout << "4.Fullscreen mode" << std::endl;
		std::cout << "5.ChunkGenerator" << std::endl;
		std::cout << "6.Exit" << std::endl;
		
		choice = _getch();
		 switch (choice)
		 {
		  case 49: exitEngine = false;LoadCubeLevel();break;
		  case 50: exitEngine = false;LoadAsciiLevel();break;
		  case 51: exitEngine = false;LoadModel();break;
		  case 52: exitEngine = false;renderEngine->GoFullScreen();break;
		  case 53: exitEngine = false;GenerateChunk();
		  case 54: return;
		  default: system("cls");
		 }
	}
}

void WolfEngine::MainLoop()
{
	float timeSinceLastUpdate = 0;
	int countFrames = 0;
	int framePerSecond = 0;
	while (!exitEngine)
	{
		//BROKEN
		//FIXED UPDATE
		//auto t_start = std::chrono::high_resolution_clock::now();
	//	auto t_end = std::chrono::high_resolution_clock::now();
	//	float timePassed = std::chrono::duration<double, std::milli>(t_end - t_start).count();
	//	timeSinceLastUpdate += timePassed;
		
		//framePerSecond = 1/(timePassed/countFrames);
	/*	while (timeSinceLastUpdate >= timePerFrame)
		{
			frames = std::to_string(framePerSecond) + "FPS";

			SetConsoleTitleA(frames.c_str());

			countFrames = 1;
			timeSinceLastUpdate -= timePerFrame;
			for (size_t i = 0; i < objects.size();i++)
			{
				for (auto ii = objects[i]->component_list.begin(); ii != objects[i]->component_list.end(); ii++)
				{
					ii->second->FixedUpdate();
				}
			}

			renderEngine->MainRenderEngineUpdate();
		}*/
		

			for (size_t i = 0; i < objects.size();i++)
			{
				for (auto ii = objects[i]->component_list.begin(); ii != objects[i]->component_list.end(); ii++)
				{
					ii->second->Update();
				}
			}
			countFrames++;
			renderEngine->MainRenderEngineUpdate();
		
		
	 //TOO SLOW
		//EVENT UPDATE
	  /*if (_kbhit())
		{
			char znak = _getch();
			if (znak == 'e')
			{
				system("cls");
				UnloadScene();
				exitEngine = true;
			}
		}*/
	}
}

void WolfEngine::UnloadScene()
{
	objects.clear();
	renderEngine->renderers.clear();
}


void WolfEngine::LoadCubeLevel()
{
	cube = new Object();
	camera = new Object();
	Camera *cam = new Camera(renderEngine->screenSize);
	camera->transform->position = Vector3(0,0, -40);
	camera->transform->rotation->eulerAngles = Vector3(0,0, 0);
	camera->AddComponent(Camera::componentName, cam);
	
	
	cube->transform->position = Vector3(0, 0, 0);
	cube->transform->rotation->eulerAngles = Vector3(0, 0, 0);
	Renderer *renderer = new Renderer();
	Mesh mesh = Primitives::CreateBox(2);
	renderer->mesh = &mesh;
    cube->AddComponent(Renderer::componentName, renderer);

	renderEngine->InitEngine();
	MainLoop();

}

void WolfEngine::LoadAsciiLevel()
{
	Object *test2 = new Object();
	Object *test3 = new Object();
	Object *test4 = new Object();
	cube = new Object();
	Object* test = new Object();
	
	camera = new Object();
	Camera *cam = new Camera(renderEngine->screenSize);
	camera->transform->position = Vector3(0, 0, -12);
	camera->AddComponent(Camera::componentName, cam);



	cube->transform->position = Vector3(12, 0, 0);
	cube->transform->rotation->eulerAngles = Vector3(0, 0, 0);
	Renderer *renderer = new Renderer();
	//renderer->pixel->Char.AsciiChar = 219;
	renderer->pixel->Attributes = 006;
	Mesh mesh = Primitives::CreateBox(2);
	renderer->mesh = &mesh;
	cube->AddComponent(Renderer::componentName, renderer);

	renderer = new Renderer();
	//renderer->pixel->Char.AsciiChar = 219;
	renderer->pixel->Attributes = 002;
	test3->transform->position = Vector3(0, 0, 18);
	mesh = Primitives::CreateBox(2);
	renderer->mesh = &mesh;
	test3->AddComponent(Renderer::componentName, renderer);

	renderer = new Renderer();
	//renderer->pixel->Char.AsciiChar = 219;
	renderer->pixel->Attributes = 004;
	test->transform->position = Vector3(6, 0, 6);
	mesh = Primitives::CreateBox(2);
	renderer->mesh = &mesh;
	test->AddComponent(Renderer::componentName, renderer);

	renderer = new Renderer();
	//renderer->pixel->Char.AsciiChar = 219;
	renderer->pixel->Attributes = 004;
	test2->transform->position = Vector3(-6, 0, 6);
	mesh = Primitives::CreateBox(2);
	renderer->mesh = &mesh;
	test2->AddComponent(Renderer::componentName, renderer);

	renderer = new Renderer();
	//renderer->pixel->Char.AsciiChar = 219;
	renderer->pixel->Attributes = 006;
	test4->transform->position = Vector3(-12, 0, 0);
	mesh = Primitives::CreateBox(2);
	renderer->mesh = &mesh;
	test4->AddComponent(Renderer::componentName, renderer);

	
	

	renderEngine->InitEngine();
	MainLoop();
}

void WolfEngine::LoadModel()
{
	cube = new Object();
	camera = new Object();
	Camera *cam = new Camera(renderEngine->screenSize);
	camera->transform->position = Vector3(0, 0, -14);
	camera->AddComponent(Camera::componentName, cam);

	cube->transform->position = Vector3(0, 0, 0);
	cube->transform->rotation->eulerAngles = Vector3(0, 0, 0);
	Renderer *renderer = new Renderer();
	renderer->pixel->Char.AsciiChar = 065;
	renderer->pixel->Attributes = 004;
	Mesh mesh;
	
	//BODY
	mesh.AddVertex(Vector3(-2, -3, 2));
	mesh.AddVertex(Vector3(2, -3, 2));
	mesh.AddVertex(Vector3(2, 1, 2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(2, 1, 2));
	mesh.AddVertex(Vector3(-2, 1, 2));
	mesh.AddVertex(Vector3(-2, -3, 2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-2, -3, 3));
	mesh.AddVertex(Vector3(2, -3, 3));
	mesh.AddVertex(Vector3(2, 1, 3));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(2, 1, 3));
	mesh.AddVertex(Vector3(-2, 1, 3));
	mesh.AddVertex(Vector3(-2, -3, 3));
	mesh.AddTriangle(0);

	//BODY SIDE
	mesh.AddVertex(Vector3(2, -3, 2));
	mesh.AddVertex(Vector3(2, -3, 3));
	mesh.AddVertex(Vector3(-2, -3, 3));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-2, -3, 3));
	mesh.AddVertex(Vector3(-2, -3, 2));
	mesh.AddVertex(Vector3(2, -3, 2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(2, 1, 2));
	mesh.AddVertex(Vector3(2, 1, 3));
	mesh.AddVertex(Vector3(-2, 1, 3));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-2, 1, 3));
	mesh.AddVertex(Vector3(-2, 1, 2));
	mesh.AddVertex(Vector3(2, -1, 2));
	mesh.AddTriangle(0);
	
	//HEAD
	mesh.AddVertex(Vector3(1, -3, 2.2));
	mesh.AddVertex(Vector3(-1, -3, 2.2));
	mesh.AddVertex(Vector3(-1, -4.5, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-1, -4.5, 2.2));
	mesh.AddVertex(Vector3(1, -4.5, 2.2));
	mesh.AddVertex(Vector3(1, -3, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(1, -3, 2.8));
	mesh.AddVertex(Vector3(-1, -3, 2.8));
	mesh.AddVertex(Vector3(-1, -4.5, 2.8));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-1, -4.5, 2.8));
	mesh.AddVertex(Vector3(1, -4.5, 2.8));
	mesh.AddVertex(Vector3(1, -3, 2.8));
	mesh.AddTriangle(0);

	//HEAD SIDE
	mesh.AddVertex(Vector3(1, -3, 2.8));
	mesh.AddVertex(Vector3(1, -3, 2.2));
	mesh.AddVertex(Vector3(1, -4.5, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(1, -4.5, 2.2));
	mesh.AddVertex(Vector3(1, -4.5, 2.8));
	mesh.AddVertex(Vector3(1, -3, 2.8));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-1, -3, 2.8));
	mesh.AddVertex(Vector3(-1, -3, 2.2));
	mesh.AddVertex(Vector3(-1, -4.5, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-1, -4.5, 2.2));
	mesh.AddVertex(Vector3(-1, -4.5, 2.8));
	mesh.AddVertex(Vector3(-1, -3, 2.8));
	mesh.AddTriangle(0);

	//RIGHT ARM
	mesh.AddVertex(Vector3(2, -2.8, 2.2));
	mesh.AddVertex(Vector3(4.4, -2.8, 2.2));
	mesh.AddVertex(Vector3(4.4, -2.2, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(4.4, -2.2, 2.2));
	mesh.AddVertex(Vector3(2, -2.2, 2.2));
	mesh.AddVertex(Vector3(2, -2.8, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(2, -2.8, 2.8));
	mesh.AddVertex(Vector3(4.4, -2.8, 2.8));
	mesh.AddVertex(Vector3(4.4, -2.2, 2.8));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(4.4, -2.2, 2.8));
	mesh.AddVertex(Vector3(2, -2.2, 2.8));
	mesh.AddVertex(Vector3(2, -2.8, 2.8));
	mesh.AddTriangle(0);


	mesh.AddVertex(Vector3(2, -2.8, 2.8));
	mesh.AddVertex(Vector3(2, -2.8, 2.2));
	mesh.AddVertex(Vector3(2, -2.2, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(2, -2.2, 2.2));
	mesh.AddVertex(Vector3(2, -2.2, 2.8));
	mesh.AddVertex(Vector3(2, -2.8, 2.8));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(4.4, -2.8, 2.8));
	mesh.AddVertex(Vector3(4.4, -2.8, 2.2));
	mesh.AddVertex(Vector3(4.4, -2.2, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(4.4, -2.2, 2.2));
	mesh.AddVertex(Vector3(4.4, -2.2, 2.8));
	mesh.AddVertex(Vector3(4.4, -2.8, 2.8));
	mesh.AddTriangle(0);

	//LEFT ARM

	mesh.AddVertex(Vector3(-2, -2.8, 2.2));
	mesh.AddVertex(Vector3(-4.4, -2.8, 2.2));
	mesh.AddVertex(Vector3(-4.4, -2.2, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-4.4, -2.2, 2.2));
	mesh.AddVertex(Vector3(-2, -2.2, 2.2));
	mesh.AddVertex(Vector3(-2, -2.8, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-2, -2.8, 2.8));
	mesh.AddVertex(Vector3(-4.4, -2.8, 2.8));
	mesh.AddVertex(Vector3(-4.4, -2.2, 2.8));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-4.4, -2.2, 2.8));
	mesh.AddVertex(Vector3(-2, -2.2, 2.8));
	mesh.AddVertex(Vector3(-2, -2.8, 2.8));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-2, -2.8, 2.8));
	mesh.AddVertex(Vector3(-2, -2.8, 2.2));
	mesh.AddVertex(Vector3(-2, -2.2, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-2, -2.2, 2.2));
	mesh.AddVertex(Vector3(-2, -2.2, 2.8));
	mesh.AddVertex(Vector3(-2, -2.8, 2.8));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-4.4, -2.8, 2.8));
	mesh.AddVertex(Vector3(-4.4, -2.8, 2.2));
	mesh.AddVertex(Vector3(-4.4, -2.2, 2.2));
	mesh.AddTriangle(0);

	mesh.AddVertex(Vector3(-4.4, -2.2, 2.2));
	mesh.AddVertex(Vector3(-4.4, -2.2, 2.8));
	mesh.AddVertex(Vector3(-4.4, -2.8, 2.8));
	mesh.AddTriangle(0);

	renderer->mesh = &mesh;
	cube->AddComponent(Renderer::componentName, renderer);

	renderEngine->InitEngine();
	MainLoop();
}

void WolfEngine::GenerateChunk()
{
	camera = new Object();
	Renderer *renderer;
	Mesh mesh;
	Camera *cam = new Camera(renderEngine->screenSize);
	camera->transform->position = Vector3(0, 0, -40);
	camera->transform->rotation->eulerAngles = Vector3(0, 0, 0);
	camera->AddComponent(Camera::componentName, cam);

	for (int x = 0;x < 8;x++)
	{
		for (int y = 0 ;y < 8;y++)
		{
			for (int z = 0;z <8;z++)
			{
				cube = new Object();
				
				cube->transform->position = Vector3(x*2, y*2, z*2);
				cube->transform->rotation->eulerAngles = Vector3(0, 0, 0);
				renderer = new Renderer();
				renderer->pixel->Attributes = x;
				mesh = Primitives::CreateBox(1.0f);
				renderer->mesh = &mesh;
				cube->AddComponent(Renderer::componentName, renderer);


				
			}
		}
   }
	renderEngine->InitEngine();
	MainLoop();
}


void WolfEngine::DrawFps()
{

	for (std::string::size_type i = 0; i < frames.size(); ++i) {
		FPSPixel->Char.AsciiChar = frames[i];
		FPSPixel->Attributes = 007;
		pos->x = 4 + i;
		pos->y = renderEngine->resolution_y - 4;

		renderEngine->DrawPixel(*pos, FPSPixel);
	}
}

WolfEngine * WolfEngine::mainEngine = NULL;
