#pragma once
#include "stdafx.h"
#include <iostream>
#include <string>
class  Object;
class Component
{
	
public:
	Component();

	Object *object;

	static std::string componentName;

	virtual void AssignObject() { std::cout << "ComponentAssigned" << std::endl; };
	virtual void virtual Update();
	virtual void virtual FixedUpdate();
};