#pragma once
#include "stdafx.h"
#include <vector>
#include "Primitives.h"
#include <Windows.h>

class RenderEngine;
class Object;
class WolfEngine
{
private:
	Object *cube;
	Object *camera;

	float timePerFrame;

	bool exitEngine = false;

	void LoadCubeLevel();
	void LoadAsciiLevel();
	void LoadModel();
	void UnloadScene();
	void GenerateChunk();

	
	string frames;


	void MainLoop();

public:
	WolfEngine();
	WolfEngine(int fixedTimeStep);

	static WolfEngine *mainEngine;
	RenderEngine *renderEngine;
	void Init();

	std::vector<Object*> objects;
	int fixedTimeStep = 60;
	

	void DrawFps();//temporary
	CHAR_INFO *FPSPixel; //temporary
	Vector2 *pos; //temporary
	
};