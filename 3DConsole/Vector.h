#pragma once
#include "stdafx.h"
#include <math.h>

//REWRITE ( VECTOR3 : PUBLIC VECTOR2 )
class Vector3
{
public: 
	Vector3(float xx, float yy, float zz);
	Vector3();

	static Vector3 Normalize(Vector3 vec);
	static bool Equal(Vector3 a, Vector3 b);
	static Vector3 Multiply(Vector3 vec, float value);
	static Vector3 Div(Vector3 vec, Vector3 value);
	static Vector3 Sub(Vector3 a, Vector3 b);
	static Vector3 ToRadians(Vector3 eulerAngles);

	void Add(Vector3 v);
	void Sub(Vector3 v);


	float x;
	float y;
	float z;
};

class Vector2
{
public:
	Vector2(float xx, float yy);
	Vector2();

	static Vector2 Normalize(Vector2 vec);
	static Vector2 Multiply(Vector2 vec, float value);
	static Vector2 Div(Vector2 vec, Vector2 value);
	static Vector2 Sub(Vector2 a, Vector2 b);


	void Add(Vector2 v);
	void Sub(Vector2 v);

	float x;
	float y;

};