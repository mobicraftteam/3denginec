#include "stdafx.h"
#include "Renderer.h"
#include <conio.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>
#include "Object.h"
#include "RenderEngine.h"
#include "Camera.h"

string Renderer::componentName = "Renderer";

Renderer::Renderer()
{
	localCenter = Vector3(0,0,0);
    
	pixel = new CHAR_INFO();
	pixel->Char.AsciiChar = 065;
	pixel->Attributes = 007;
}

void Renderer::AssignObject()
{
	//std::cout << "RendererAssigned" << std::endl;
	renderEngine = object->mainEngine->renderEngine;
	renderEngine->AddRenderer(this);

}

void Renderer::AddMesh(Mesh *m)
{
	mesh = m;
}

void Renderer::CalculateMesh()
{
	float minX = 0;
	float maxX = 0;
	float minY = 0;
	float maxY = 0;
}

Vector3 Renderer::getCurrentCenter()
{
	return Vector3(localCenter.x + object->transform->position.x, localCenter.y + object->transform->position.y, localCenter.z + object->transform->position.z);
}


void Renderer::RenderMesh()
{
	for (unsigned int i = 0; i < mesh->triangles.size(); i++)
	{
		Vector3 points[3];
		points[0] = mesh->vertexs[(i * 3)];
		points[1] = mesh->vertexs[(i * 3) + 1];
		points[2] = mesh->vertexs[(i * 3) + 2];

		//Z ROTATION

		Vector3 rotatedPointsZ[3];
		Vector3 rotatedPointsX[3];
		Vector3 rotatedPointsY[3];


		for (int rO = 0;rO < 3;rO++)
		{
			rotatedPointsZ[rO].x = ((points[rO].x - localCenter.x) *cos(object->transform->rotation->eulerAngles.z* 3.14159 / 180)) - ((points[rO].y-localCenter.y) * sin(object->transform->rotation->eulerAngles.z* 3.14159 / 180) + localCenter.x);
			rotatedPointsZ[rO].y = ((points[rO].x - localCenter.x) *sin(object->transform->rotation->eulerAngles.z* 3.14159 / 180)) + ((points[rO].y-localCenter.y) * cos(object->transform->rotation->eulerAngles.z* 3.14159 / 180) + localCenter.y);
			rotatedPointsZ[rO].z = points[rO].z;
		}
		
		for (int rO = 0;rO < 3;rO++)
		{
			rotatedPointsX[rO].y = ((rotatedPointsZ[rO].y - localCenter.y) *cos(object->transform->rotation->eulerAngles.x* 3.14159 / 180)) - ((rotatedPointsZ[rO].z - localCenter.z) * sin(object->transform->rotation->eulerAngles.x* 3.14159 / 180) + localCenter.y);
			rotatedPointsX[rO].z = ((rotatedPointsZ[rO].y - localCenter.y) *sin(object->transform->rotation->eulerAngles.x* 3.14159 / 180)) + ((rotatedPointsZ[rO].z - localCenter.z) * cos(object->transform->rotation->eulerAngles.x* 3.14159 / 180) + localCenter.z);
			rotatedPointsX[rO].x = rotatedPointsZ[rO].x;
		}
		
		for (int rO = 0;rO < 3;rO++)
		{
			rotatedPointsY[rO].z = ((rotatedPointsX[rO].z - localCenter.z) *cos(object->transform->rotation->eulerAngles.y* 3.14159 / 180)) - ((rotatedPointsX[rO].x - localCenter.x) * sin(object->transform->rotation->eulerAngles.y* 3.14159 / 180) + localCenter.z);
			rotatedPointsY[rO].x = ((rotatedPointsX[rO].z - localCenter.z) *sin(object->transform->rotation->eulerAngles.y* 3.14159 / 180)) + ((rotatedPointsX[rO].x - localCenter.x) * cos(object->transform->rotation->eulerAngles.y* 3.14159 / 180) + localCenter.x);
			rotatedPointsY[rO].y = rotatedPointsX[rO].y;
		}

		rotatedPointsY[0].Add(object->transform->position);
		rotatedPointsY[1].Add(object->transform->position);
		rotatedPointsY[2].Add(object->transform->position);
		
		renderEngine->mainCamera->CameraRenderTriangle(&rotatedPointsY[0], pixel);
	}
}

void Renderer::FixedUpdate()
{
	
	//object->transform->rotation->eulerAngles.Add(Vector3(1, 0, 2));
}

void Renderer::Update()
{
	object->transform->rotation->eulerAngles.Add(Vector3(0.1f, 0, 0.2f));
}

