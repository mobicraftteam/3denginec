#include "stdafx.h"
#include "Transform.h"
#include <iostream>

Transform::Transform()
{
	parent = NULL;
	position = Vector3(0,0,0);
	localPosition = Vector3(0,0,0);
	rotation = new Quaternion();
	rotation->eulerAngles = Vector3(0, 0, 0);
}

void Transform::AssignObject()
{
	//std::cout << "TransformAssigned" << std::endl;
}

void Transform::RotateAround(Vector3 *p, Vector3 *rotation)
{
	Vector3 rotatedPosition;
		rotatedPosition.z = ((rotatedPosition.z - p->z) *cos(rotation->y* 3.14159 / 180)) - ((rotatedPosition.x - p->x) * sin(rotation->y* 3.14159 / 180) + p->z);
		rotatedPosition.x = ((rotatedPosition.z - p->z) *sin(rotation->y* 3.14159 / 180)) + ((rotatedPosition.x - p->x) * cos(rotation->y* 3.14159 / 180) + p->x);
		rotatedPosition.y = position.y;
	
		position = rotatedPosition;
}

Vector3 Transform::DegreeToRadians()
{
 return Vector3::Multiply(rotation->eulerAngles, 3.14159 / 180);
}

Vector3 Transform::GetForwardVector()
{
	return Vector3(sin(DegreeToRadians().y), -(sin(DegreeToRadians().x)*cos(DegreeToRadians().y)), (cos(DegreeToRadians().x)*cos(DegreeToRadians().y)));

}