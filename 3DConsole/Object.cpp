#include "stdafx.h"
#include "Object.h"
#include <math.h>
#include <iostream>
#include <conio.h>

Object::Object()
{
	transform = new Transform();
	mainEngine = WolfEngine::mainEngine;
	mainEngine->objects.push_back(this);
	AddComponent("Transform", transform);
}

void Object::AddComponent(std::string name, Component *c)
{
	component_list[name] = c;
	c->object = this;
	c->AssignObject();
}

void Object::RemoveComponent(std::string t)
{
	if (component_list.find(t) == component_list.end())
	{
		return;
	}
	else
	{
		component_list.erase(t);
		return;
	}
}

Component* Object::GetComponent(std::string t)
{
	if (component_list.find(t) == component_list.end())
	{
		return NULL;
	}
	else
	{
		return component_list[t];
	}
}



