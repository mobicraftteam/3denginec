#pragma once
#include "stdafx.h"
#include <iostream>
#include <unordered_map>
#include <typeindex>
#include <typeinfo>
#include "WolfEngine.h"
#include "Component.h"
#include "Transform.h"
#include <string>

class WolfEngine;
class Component;



class Object
{
private:
	

public:
	Object();
	std::unordered_map<std::string, Component*> component_list;

	std::string name;

	WolfEngine *mainEngine;
	Transform *transform; 

	void AddComponent(std::string name, Component *c);
	void RemoveComponent(std::string t);
	Component* GetComponent(string name);

   
	
	

};

