#include "stdafx.h"
#include "Vector.h"
#include <math.h>


Vector3::Vector3(float xx, float yy, float zz)
{
	x = xx;
	y = yy;
	z = zz;
}

Vector3::Vector3()
{
	x = 0;
	y = 0;
	z = 0;
}

Vector3 Vector3::Multiply(Vector3 vec, float value)
{
	return Vector3(vec.x*value, vec.y*value, vec.z*value);
}

Vector3 Vector3::Div(Vector3 vec, Vector3 value)
{
	return Vector3(vec.x/value.x, vec.y/value.y, vec.z/value.z);
}

Vector3 Vector3::Sub(Vector3 a, Vector3 b)
{
	return Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
}

bool Vector3::Equal(Vector3 a, Vector3 b)
{
	if (a.x != b.x)
	{
		return false;
	}

	if (a.y != b.y)
	{
		return false;
	}

	if (a.z != b.z)
	{
		return false;
	}

	return true;
}

Vector3 Vector3::Normalize(Vector3 v)
{
	float lenght = sqrt((v.x*v.x) + (v.y*v.y) + (v.z*v.z));

	Vector3 normalized;
	if (lenght == 0)
	{
		normalized.x = 0;
		normalized.y = 0;
		normalized.z = 0;
	}
	else
	{
		normalized.x = v.x / lenght;
		normalized.y = v.y / lenght;
		normalized.z = v.z / lenght;
	}


	return normalized;
}

void Vector3::Add(Vector3 v)
{
	x = x + v.x;
	y = y + v.y;
	z = z + v.z;
}

void Vector3::Sub(Vector3 v)
{
	x = x - v.x;
	y = y - v.y;
	z = z - v.z;
	
}

Vector3 Vector3::ToRadians(Vector3 eulerAngles)
{
	return Vector3::Multiply(eulerAngles, 3.14159 / 180);
}

Vector2::Vector2(float xx, float yy)
{
	x = xx;
	y = yy;
}

Vector2::Vector2()
{

}

Vector2 Vector2::Normalize(Vector2 v)
{
	float lenght = sqrt((v.x*v.x) + (v.y*v.y));

	Vector2 normalized = v;
	if (lenght == 0)
	{
		normalized.x = 0;
		normalized.y = 0;
	}
	else
	{
		normalized.x = v.x / lenght;
		normalized.y = v.y / lenght;
	}

	return normalized;
}

void Vector2::Add(Vector2 v)
{
	x = x + v.x;
	y = y + v.y;
}

void Vector2::Sub(Vector2 v)
{
	x = x - v.x;
	y = y - v.y;
}

Vector2 Vector2::Multiply(Vector2 vec, float value)
{
	return Vector2(vec.x*value, vec.y*value);
}

Vector2 Vector2::Div(Vector2 vec, Vector2 value)
{
	if (value.x == 0)
	{
		value.x = 1;
		vec.x = 0;
	}
	if (value.y == 0)
	{
		value.y = 1;
		vec.y = 0;
	}
	
	return Vector2(vec.x / value.x, vec.y / value.y);
}

Vector2 Vector2::Sub(Vector2 a, Vector2 b)
{
	return Vector2(a.x - b.x, a.y - b.y);
}