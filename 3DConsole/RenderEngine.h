#pragma once
#include "stdafx.h"
#include <map>
#include <stdlib.h>
#include <Windows.h>
#include <Tchar.h>
#include <vector>
#include <time.h>
#include <ctime>
#include <functional>
#include <thread>
#include "Vector.h"

using namespace std;

//219

class Renderer;
class Camera;

class RenderEngine
{
private:


	COORD bufferSize;
	COORD charBufSize;
	COORD characterPos;
	SMALL_RECT writeArea;
	CHAR_INFO clearBuffer[500 * 300];

	int pixelDrawPos;

	void Draw();
	void Render();
	void ClearBuffer();

	int width;
	int height;

public:

	Vector2 screenSize;

	Camera *mainCamera;
	bool hasCamera = false;

	vector<Renderer*> renderers;
	map<Vector2, float> filledPixels;

	RenderEngine(int w, int h, bool mthreadRendering);
	void InitEngine();

	CHAR_INFO screenBuffer[500 * 300];
	HANDLE wHnd;    // Handler to write to the console.
	HANDLE rHnd;    // Handler to read from the console.


	SHORT resolution_x;
	SHORT resolution_y;

	void MainRenderEngineUpdate();
	void DrawPixel(Vector2 pos, CHAR_INFO *pixel);
    void AddRenderer(Renderer *r);
	void GoFullScreen();
};