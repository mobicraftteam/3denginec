#pragma once
#include "stdafx.h"
#include <vector>
#include "Vector.h"


using namespace std;



class Mesh
{
private:

public:
	Mesh();

	vector<Vector3> vertexs;
	vector<int> triangles;
	
	void AddVertex(Vector3 vtX);
	void AddTriangle(int triangle);

};