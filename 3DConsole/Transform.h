#pragma once
#include "stdafx.h"
#include "Vector.h"
#include "Component.h"
#include "Quaternion.h"


class Transform : public Component
{
public:
	Vector3 position;
	Vector3 localPosition;
	Quaternion *rotation;

	Vector3 forward;

	Transform *parent;
	Transform();

//CORE FUNCTIONS//////
	void AssignObject();
//////////////////////
	void RotateAround(Vector3 *point, Vector3 * rotation);
	Vector3 DegreeToRadians();
	Vector3 GetForwardVector();
};