========================================================================
    3DCONSOLEENGINE ver 0.1 - Created by RedHatWolf - Artur Czapiewski
========================================================================
                      www.wolfstudio.xyz
		    www.czapiewski.students.wmi.amu.edu.pl

	Dev notes(ver 0.1):
	  - basic perspective camera
	  - drawing triangles
	  - basic rotation based on euler angles(no quaternions)
	  - z buffer
	  - basic reading file system


	TO DO:
	  - FIX MEMORY LEAKS
	  - FIX CAMERA FLIPPED Z AXIS
	  - move all calculations to matrix4x4
	  
	  - textures and materials
	  - multithreading
	  - event system
	  - scene manager
	  - ui
	  - physic

	  If you have any question you can reach out to me using one of this emails:
	   - redhatwolf@wolfstudio.xyz
	   - artur.czapiewski@yahoo.com
/////////////////////////////////////////////////////////////////////////////
