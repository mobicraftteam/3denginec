#include "stdafx.h"
#include "Camera.h"
#include "RenderEngine.h"
#include "Object.h"
#include <math.h>
#include <conio.h>

# define M_PI           3.14159265358979323846
# define SQRT           0.8660254

string Camera::componentName = "Camera";

Camera::Camera(Vector2 sC)
{
	screenSize = sC;
	for (int i = 0;i < sC.x;i++)
	{
		for (int j = 0;j < sC.y;j++)
		{
			z_buff[i][j] = 0;
		}
	}
}

void Camera::AssignObject()
{
	
	cout << "CameraAssgned" << endl;
	renderEngine = WolfEngine::mainEngine->renderEngine;
	if (renderEngine->hasCamera == false)
	{
		renderEngine->hasCamera = true;
		renderEngine->mainCamera = this;
	}
}

void Camera::DrawPixel(Vector2 pos, CHAR_INFO *pixel)
{
	renderEngine->DrawPixel(pos, pixel);
}

Vector2 Camera::WorldToScreenPosition(Vector3 *pos)
{
	Vector3 c = Vector3(object->transform->position);
	Vector3 angles = object->transform->rotation->eulerAngles;

	angles.Add(Vector3(0, 0, 0));
	Vector3 radians = Vector3::ToRadians(angles);

	float x = pos->x - c.x;
	float y = pos->y - c.y;
	float z = pos->z - c.z;

	z_leng = x*x + y*y + z*z / 100.0f;

	float xDir = (cos(radians.y)*((sin(radians.z)*y) + (cos(radians.z)*x))) - sin(radians.y)*z;
	float yDir = (sin(radians.x)*((cos(radians.y)*z) + (sin(radians.y)*((sin(radians.z)*y) + (cos(radians.z)*x))))) + (cos(radians.x)*((cos(radians.z)*y) - (sin(radians.z)*x)));
	float zDir = (cos(radians.x)*((cos(radians.y)*z) + (sin(radians.y)*((sin(radians.z)*y) + (cos(radians.z)*x))))) - (sin(radians.x)*((cos(radians.z)*y) - (sin(radians.z)*x)));

	float average_len = M_PI * 55;
	Vector2 screenPoint;

	if (!CheckIfPointIsBehindeCamera(pos))
	{
		if (zDir != 0)
		{
			screenPoint.x = round(xDir * (average_len / zDir));
			screenPoint.y = round(yDir * (average_len / zDir));
			screenPoint.Add(Vector2((renderEngine->resolution_x - 1) / 2, (renderEngine->resolution_y) / 2));
		}
	}

		return screenPoint;
}

void Camera::CameraRenderTriangle(Vector3 *point, CHAR_INFO *pixelData)
{
	if (!CheckIfPointIsBehindeCamera(point))
	{
		Vector2 screenPoints[3];
		screenPoints[0] = WorldToScreenPosition(point);
		screenPoints[1] = WorldToScreenPosition(point + 1);
		screenPoints[2] = WorldToScreenPosition(point + 2);

		Vector2 directions[3];
		directions[0] = Vector2::Normalize(Vector2(screenPoints[1].x - screenPoints[0].x, screenPoints[1].y - screenPoints[0].y));
		directions[1] = Vector2::Normalize(Vector2(screenPoints[2].x - screenPoints[1].x, screenPoints[2].y - screenPoints[1].y));
		directions[2] = Vector2::Normalize(Vector2(screenPoints[2].x - screenPoints[0].x, screenPoints[2].y - screenPoints[0].y));

		renderEngine->DrawPixel(screenPoints[0], pixelData);
		renderEngine->DrawPixel(screenPoints[1], pixelData);
		renderEngine->DrawPixel(screenPoints[2], pixelData);

		int step[3];
		if (directions[0].x != 0)
		{
			step[0] = (screenPoints[1].x - screenPoints[0].x) / directions[0].x;
		}
		else if (directions[0].y != 0)
		{
			step[0] = (screenPoints[1].y - screenPoints[0].y) / directions[0].y;
		}
		else
		{
			step[0] = 0;
		}

		if (directions[1].x != 0)
		{
			step[1] = (screenPoints[2].x - screenPoints[1].x) / directions[1].x;
		}
		else if (directions[1].y != 0)
		{
			step[1] = (screenPoints[2].y - screenPoints[1].y) / directions[1].y;
		}
		else
		{
			step[1] = 0;
		}

		if (directions[2].x != 0)
		{
			step[2] = (screenPoints[2].x - screenPoints[0].x) / directions[2].x;
		}
		else if (directions[2].y != 0)
		{
			step[2] = (screenPoints[2].y - screenPoints[0].y) / directions[2].y;
		}
		else
		{
			step[2] = 0;
		}

		for (int vertexCount = 0;vertexCount < 2;vertexCount++)
		{
			for (int i = 0;i < step[vertexCount] - 1;i++)
			{
				screenPoints[vertexCount].Add(Vector2((directions[vertexCount].x), (directions[vertexCount].y)));
				renderEngine->DrawPixel(Vector2(round(screenPoints[vertexCount].x), round(screenPoints[vertexCount].y)), pixelData);
			}
		}
	}
}

bool Camera::CheckIfPointIsBehindeCamera(Vector3 * point)
{
	return false;
}


void Camera::FixedUpdate()
{

}

void Camera::Update() 
{
	if (_kbhit())
	{
		char znak = _getch();
		if (znak == 'w')
		{
			object->transform->position.Add(object->transform->GetForwardVector());
		}
		if (znak == 's')
		{
			object->transform->position.Add(Vector3::Multiply(object->transform->GetForwardVector(), -1.0f));
		}
		if (znak == 'a')
		{
			object->transform->position.Add(Vector3(-0.4f, 0, 0));
		}
		if (znak == 'd')
		{
			object->transform->position.Add(Vector3(0.4f, 0, 0));
		}

		if (znak == 'q')
		{
			object->transform->position.Add(Vector3(0, -0.2f, 0));
		}
		if (znak == 'e')
		{
			object->transform->position.Add(Vector3(0, 0.2f, 0));
		}

		if (znak == 'j')
		{
			object->transform->rotation->eulerAngles.Add(Vector3(0, -1.0f, 0));
		}
		if (znak == 'l')
		{
			object->transform->rotation->eulerAngles.Add(Vector3(0, 1.0f, 0));
		}

		if (znak == 'i')
		{
			object->transform->rotation->eulerAngles.Add(Vector3(1.0f,0, 0));
		}

		if (znak == 'k')
		{
			object->transform->rotation->eulerAngles.Add(Vector3(-1.0f, 0, 0));
		}
	}
	
}