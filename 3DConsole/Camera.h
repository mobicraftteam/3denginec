#pragma once
#include "stdafx.h"
#include <Windows.h>
#include "Component.h"
#include "Vector.h"
#include <conio.h>


class RenderEngine;
class Camera : public Component
{
private:
	RenderEngine *renderEngine;
	Vector2 screenSize;
	float z_buff[500][300];
	float z_leng = 0;

	float distance = 10;

	void DrawPixel(Vector2 pos, CHAR_INFO *pixelData);
    Vector2 WorldToScreenPosition(Vector3 *pos);
	bool CheckIfPointIsBehindeCamera(Vector3 * pos);
public:

	Camera(Vector2 screenSize);
	static std::string componentName;

	void AssignObject();

	void CameraRenderTriangle(Vector3 *point, CHAR_INFO *pixelData);
	void FixedUpdate();
	void Update();
};