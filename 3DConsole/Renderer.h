#pragma once
#include "stdafx.h"
#include "Vector.h"
#include "Mesh.h"
#include <stdlib.h>
#include <Windows.h>
#include <Tchar.h>
#include <algorithm>
#include <iostream>
#include "Component.h"



using namespace std;

class Object;
class RenderEngine;

class Renderer : public Component
{
private:
	RenderEngine * renderEngine;
	bool renderTriangles = true;
	Vector3 localCenter;

	

//RENDER MESH FUNCTION
Vector3 point1;
Vector3 point2;
Vector3 point3;

Vector3 *vec1Norm;
Vector3 *vec2Norm;
Vector3 *vec3Norm;
Vector3 tempCursorPos;

int step;
float rad;
float xPixel;
float yPixel;

int drawOnX;
int drawOnY;
int drawOnZ;
//

public:
	Renderer();
	void AssignObject();

	static std::string componentName;

	Mesh *mesh;

	CHAR_INFO *pixel;

	Vector3 getCurrentCenter();

	void AddMesh(Mesh *mesh);
	void CalculateMesh();
	void RenderMesh();

	void FixedUpdate();
	void Update();
};